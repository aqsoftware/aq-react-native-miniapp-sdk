//
//  RNFunTypeViewManager.h
//  RNFunType
//
//  Created by Ryan Brozo on 10/03/2017.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>
#import <React/RCTViewManager.h>


@interface RNFunTypeViewManager : RCTViewManager

@end
