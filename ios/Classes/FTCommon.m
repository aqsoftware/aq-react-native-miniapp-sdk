//
//  FTCommon.m
//  FunTypeCore
//
//  Created by Ryan Brozo on 02/03/2017.
//  Copyright © 2017 Bengga. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FTCommon.h"

@implementation FTFunType
@synthesize funTypeId, type, webUrl;
@end

@implementation FTWebFunTypeInfo
@synthesize url, webKitMessages;
@end
