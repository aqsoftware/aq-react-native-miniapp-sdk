//
//  RNMiniAppCore.h
//  RNMiniAppCore
//
//  Created by Ryan Brozo on 20/06/2017.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for RNMiniAppCore.
FOUNDATION_EXPORT double RNMiniAppCoreVersionNumber;

//! Project version string for RNMiniAppCore.
FOUNDATION_EXPORT const unsigned char RNMiniAppCoreVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <RNMiniAppCore/PublicHeader.h>


