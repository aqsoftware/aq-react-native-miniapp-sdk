package com.bengga.react.core;

/**
 * Created by ryanbrozo on 04/10/2017.
 */

public enum FunTypeDownload {
  ONLINE, IN_APP_NOT_DOWNLOADED, IN_APP_DOWNLOADED
}
