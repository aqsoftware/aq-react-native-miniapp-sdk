package com.bengga.core;

import java.net.URL;

/**
 * Created by ryanbrozo on 04/10/2017.
 */

public class FunType {
  public String id;
  public String name;
  public FunTypeType type;
  public URL webUrl;
  public URL packageFileUrl;
  public String packageFileHash;
}
