package com.bengga.core;

/**
 * Created by ryanbrozo on 04/10/2017.
 */

public enum FunTypeType {
  INTERNAL, EXTERNAL_NATIVE, EXTERNAL_WEB_BASED
}
